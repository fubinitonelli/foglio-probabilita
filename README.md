# foglio-probabilità

[![License: CC BY-SA 4.0](https://img.shields.io/badge/License-CC%20BY--SA%204.0-lightgrey.svg)](https://creativecommons.org/licenses/by-sa/4.0/)

Questo "schemino" di probabilità è stato creato dai fubini⊗tonelli insieme al libro _Appunti di Probabilità_. Siete invitati a modificarlo e distribuirlo, secondo i termini della licenza [Creative Commons BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/). 

Vi invitiamo a non usarlo direttamente come supporto d'esame: non vi garantiamo che sia corretto e completo.

Saremmo lieti di ricevere commenti, correzioni e miglioramenti. Buono studio!


